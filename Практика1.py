sp1 = ['a', 1, -2, 1.2,]

#Задание 1

sp2 = [x for x in sp1 if type(x) == int and x%2 == 0]
print(sp2)

#Задание 2

sp3 = sp1[::2]
print(sp3)

#Задание 3

sp4 = [x for x in sp1 if type(x) == int and x>0]
print(sp4)

#Задание 7
value = [i for i in sp1 if sp1.index(i)%2 != 0]
key = [i for i in sp1 if sp1.index(i)%2 == 0]
slov = {k:v for k, v in zip(key, value)}
print(slov)

#Задание 5
dictionary = {'b':2, 3:4, 'a':9, 4:1}
slov = [x+y for x,y in dictionary.items() if type(x) in [int, float] and type(y) in [int, float]]
print(slov)

#Задание 6
sp1 = [3, 4.1, 2]
sp1.sort()
sp2=[(sp1[i], sp1[j]) for i in range (len(sp1)-1) for j in range(i+1, len(sp1))]
print(sp2)

#Задание 4
m = 3
sp5 = [1,3,4,7,9]
mn1 = {x for x in sp5 if x%m==1}
mn2 = {x for x in sp5 if x%m==0}
sp6 = [mn2,mn1]
print(sp6)
